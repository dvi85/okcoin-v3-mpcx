package com.okcoin.commons.okex.open.api.service.spot.impl;

import com.okcoin.commons.okex.open.api.bean.spot.result.Instrument;

import java.util.List;

public interface SpotMarketAPIService {
    List<Instrument> getInstruments();
}
