package com.okcoin.commons.okex.open.api.service.spot.impl;

import com.okcoin.commons.okex.open.api.bean.spot.result.Instrument;
import com.okcoin.commons.okex.open.api.client.APIClient;
import com.okcoin.commons.okex.open.api.config.APIConfiguration;

import java.util.List;

public class SpotMarketAPIServiceImpl implements SpotMarketAPIService {

    private final APIClient client;
    private final SpotMarketApi spotMarketApi;

    public SpotMarketAPIServiceImpl(final APIConfiguration config) {
        this.client = new APIClient(config);
        this.spotMarketApi = this.client.createService(SpotMarketApi.class);
    }

    @Override
    public List<Instrument> getInstruments() {
        return this.client.executeSync(this.spotMarketApi.getInstruments());
    }
}
