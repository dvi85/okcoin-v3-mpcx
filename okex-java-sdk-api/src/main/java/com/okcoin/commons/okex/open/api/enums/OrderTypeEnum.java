package com.okcoin.commons.okex.open.api.enums;

public enum OrderTypeEnum {
    market, limit
}
