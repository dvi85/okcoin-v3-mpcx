package com.okcoin.commons.okex.open.api.enums;

public enum OrderSideEnum {
    buy, sell
}
