package com.okcoin.commons.okex.open.api.bean.spot.result;

import java.math.BigDecimal;

/**
 * futures contract products <br/>
 *
 * @author Tony Tian
 * @version 1.0.0
 * @date 2018/2/26 10:49
 */
public class Instrument {
    /**
     * The id of the futures contract
     */
    private String instrument_id;
    /**
     * Currency
     */
    private String base_currency;
    /**
     * Quote currency
     */
    private String quote_currency;
    /**
     * minimum trading size
     */
    private BigDecimal min_size;
    /**
     * minimum increment size
     */
    private BigDecimal size_increment;
    /**
     * 	trading price increment
     */
    private BigDecimal tick_size;

    public String getInstrument_id() {
        return instrument_id;
    }

    public void setInstrument_id(String instrument_id) {
        this.instrument_id = instrument_id;
    }

    public String getBase_currency() {
        return base_currency;
    }

    public void setBase_currency(String base_currency) {
        this.base_currency = base_currency;
    }

    public String getQuote_currency() {
        return quote_currency;
    }

    public void setQuote_currency(String quote_currency) {
        this.quote_currency = quote_currency;
    }

    public BigDecimal getMin_size() {
        return min_size;
    }

    public void setMin_size(BigDecimal min_size) {
        this.min_size = min_size;
    }

    public BigDecimal getSize_increment() {
        return size_increment;
    }

    public void setSize_increment(BigDecimal size_increment) {
        this.size_increment = size_increment;
    }

    public BigDecimal getTick_size() {
        return tick_size;
    }

    public void setTick_size(BigDecimal tick_size) {
        this.tick_size = tick_size;
    }
}
