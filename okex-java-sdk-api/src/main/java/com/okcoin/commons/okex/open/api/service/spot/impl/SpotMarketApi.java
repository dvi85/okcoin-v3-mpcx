package com.okcoin.commons.okex.open.api.service.spot.impl;

import com.okcoin.commons.okex.open.api.bean.spot.result.Instrument;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface SpotMarketApi {

    @GET("api/spot/v3/instruments")
    Call<List<Instrument>> getInstruments();
}
