package com.okcoin.commons.okex.open.api.test.spot;

import com.okcoin.commons.okex.open.api.bean.spot.param.OrderParamDto;
import com.okcoin.commons.okex.open.api.bean.spot.param.PlaceOrderParam;
import com.okcoin.commons.okex.open.api.bean.spot.result.*;
import com.okcoin.commons.okex.open.api.enums.OrderSideEnum;
import com.okcoin.commons.okex.open.api.enums.OrderTypeEnum;
import com.okcoin.commons.okex.open.api.service.spot.SpotOrderAPIServive;
import com.okcoin.commons.okex.open.api.service.spot.impl.SpotMarketAPIService;
import com.okcoin.commons.okex.open.api.service.spot.impl.SpotMarketAPIServiceImpl;
import com.okcoin.commons.okex.open.api.service.spot.impl.SpotMarketApi;
import com.okcoin.commons.okex.open.api.service.spot.impl.SpotOrderApiServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SpotMarketAPITest extends SpotAPIBaseTests {

    private static final Logger LOG = LoggerFactory.getLogger(SpotMarketAPITest.class);

    private SpotMarketAPIService spotOrderAPIServive;

    @Before
    public void before() {
        this.config = this.config();
        this.spotOrderAPIServive = new SpotMarketAPIServiceImpl(this.config);
    }

    @Test
    public void getInstruments() {
        final List<Instrument> instruments = this.spotOrderAPIServive.getInstruments();
        this.toResultString(SpotMarketAPITest.LOG, "orderInfoList", instruments);
    }
}
