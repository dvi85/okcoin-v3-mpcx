package com.okcoin.commons.okex.open.api.test.spot;

import com.okcoin.commons.okex.open.api.config.APIConfiguration;
import com.okcoin.commons.okex.open.api.enums.I18nEnum;
import com.okcoin.commons.okex.open.api.test.BaseTests;

public class SpotAPIBaseTests extends BaseTests {

    public APIConfiguration config() {
        final APIConfiguration config = new APIConfiguration();

        config.setEndpoint("https://www.okcoin.com/");
        // apiKey，api注册成功后页面上有
        config.setApiKey("3027ec30-82ad-40d3-8e2c-49f7c6c86d7e");
        // secretKey，api注册成功后页面上有
        config.setSecretKey("0B4888832C7D622AE90EFA8DBE7C8120");
        config.setPassphrase("RvFoWT6&xMHMvHeB5V85L&3jYTtr$5");
        config.setPrint(true);
        config.setI18n(I18nEnum.SIMPLIFIED_CHINESE);

        return config;
    }

}

/*
3027ec30-82ad-40d3-8e2c-49f7c6c86d7e
Link IP address:91.213.184.105,79.165.19.142,37.143.10.138
secretKey: 0B4888832C7D622AE90EFA8DBE7C8120
 */
